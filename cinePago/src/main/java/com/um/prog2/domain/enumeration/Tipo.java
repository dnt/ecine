package com.um.prog2.domain.enumeration;

/**
 * The Tipo enumeration.
 */
public enum Tipo {
    CREDITO, DEBITO
}
