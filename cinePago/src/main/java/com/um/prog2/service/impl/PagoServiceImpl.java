package com.um.prog2.service.impl;

import com.um.prog2.service.PagoService;
import com.um.prog2.domain.Pago;
import com.um.prog2.repository.PagoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Pago.
 */
@Service
@Transactional
public class PagoServiceImpl implements PagoService {

    private final Logger log = LoggerFactory.getLogger(PagoServiceImpl.class);

    private final PagoRepository pagoRepository;

    public PagoServiceImpl(PagoRepository pagoRepository) {
        this.pagoRepository = pagoRepository;
    }

    /**
     * Save a pago.
     *
     * @param pago the entity to save
     * @return the persisted entity
     */
    @Override
    public Pago save(Pago pago) {
        log.debug("Request to save Pago : {}", pago);        return pagoRepository.save(pago);
    }

    /**
     * Get all the pagos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Pago> findAll(Pageable pageable) {
        log.debug("Request to get all Pagos");
        return pagoRepository.findAll(pageable);
    }


    /**
     * Get one pago by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Pago> findOne(Long id) {
        log.debug("Request to get Pago : {}", id);
        return pagoRepository.findById(id);
    }

    /**
     * Delete the pago by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pago : {}", id);
        pagoRepository.deleteById(id);
    }
}
