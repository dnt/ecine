package com.um.prog2.service;

import com.um.prog2.domain.Pago;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Pago.
 */
public interface PagoService {

    /**
     * Save a pago.
     *
     * @param pago the entity to save
     * @return the persisted entity
     */
    Pago save(Pago pago);

    /**
     * Get all the pagos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Pago> findAll(Pageable pageable);


    /**
     * Get the "id" pago.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Pago> findOne(Long id);

    /**
     * Delete the "id" pago.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
