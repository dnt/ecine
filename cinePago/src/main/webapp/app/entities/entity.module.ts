import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CinePagoPagoModule } from './pago/pago.module';
import { CinePagoClienteModule } from './cliente/cliente.module';
import { CinePagoTarjetaModule } from './tarjeta/tarjeta.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        CinePagoPagoModule,
        CinePagoClienteModule,
        CinePagoTarjetaModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CinePagoEntityModule {}
