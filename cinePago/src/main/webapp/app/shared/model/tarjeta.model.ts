import { Moment } from 'moment';
import { ICliente } from 'app/shared/model//cliente.model';
import { IPago } from 'app/shared/model//pago.model';

export const enum Tipo {
    CREDITO = 'CREDITO',
    DEBITO = 'DEBITO'
}

export interface ITarjeta {
    id?: number;
    numero?: string;
    tipo?: Tipo;
    saldo?: number;
    created?: Moment;
    updated?: Moment;
    cliente?: ICliente;
    pagos?: IPago[];
}

export class Tarjeta implements ITarjeta {
    constructor(
        public id?: number,
        public numero?: string,
        public tipo?: Tipo,
        public saldo?: number,
        public created?: Moment,
        public updated?: Moment,
        public cliente?: ICliente,
        public pagos?: IPago[]
    ) {}
}
