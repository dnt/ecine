import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IFuncion } from 'app/shared/model/funcion.model';
import { FuncionService } from './funcion.service';
import { ISala } from 'app/shared/model/sala.model';
import { SalaService } from 'app/entities/sala';
import { IPelicula } from 'app/shared/model/pelicula.model';
import { PeliculaService } from 'app/entities/pelicula';

@Component({
    selector: 'jhi-funcion-update',
    templateUrl: './funcion-update.component.html'
})
export class FuncionUpdateComponent implements OnInit {
    private _funcion: IFuncion;
    isSaving: boolean;

    salas: ISala[];

    peliculas: IPelicula[];
    fecha: string;
    created: string;
    updated: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private funcionService: FuncionService,
        private salaService: SalaService,
        private peliculaService: PeliculaService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ funcion }) => {
            this.funcion = funcion;
        });
        this.salaService.query().subscribe(
            (res: HttpResponse<ISala[]>) => {
                this.salas = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.peliculaService.query().subscribe(
            (res: HttpResponse<IPelicula[]>) => {
                this.peliculas = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.funcion.fecha = moment(this.fecha, DATE_TIME_FORMAT);
        this.funcion.created = moment(this.created, DATE_TIME_FORMAT);
        this.funcion.updated = moment(this.updated, DATE_TIME_FORMAT);
        if (this.funcion.id !== undefined) {
            this.subscribeToSaveResponse(this.funcionService.update(this.funcion));
        } else {
            this.subscribeToSaveResponse(this.funcionService.create(this.funcion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IFuncion>>) {
        result.subscribe((res: HttpResponse<IFuncion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSalaById(index: number, item: ISala) {
        return item.id;
    }

    trackPeliculaById(index: number, item: IPelicula) {
        return item.id;
    }
    get funcion() {
        return this._funcion;
    }

    set funcion(funcion: IFuncion) {
        this._funcion = funcion;
        this.fecha = moment(funcion.fecha).format(DATE_TIME_FORMAT);
        this.created = moment(funcion.created).format(DATE_TIME_FORMAT);
        this.updated = moment(funcion.updated).format(DATE_TIME_FORMAT);
    }
}
