import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CineOcupacionModule } from './ocupacion/ocupacion.module';
import { CineCalificacionModule } from './calificacion/calificacion.module';
import { CineButacaModule } from './butaca/butaca.module';
import { CineFuncionModule } from './funcion/funcion.module';
import { CineSalaModule } from './sala/sala.module';
import { CineTicketModule } from './ticket/ticket.module';
import { CinePeliculaModule } from './pelicula/pelicula.module';
import { CineEntradaModule } from './entrada/entrada.module';
import { CineClienteModule } from './cliente/cliente.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        CineOcupacionModule,
        CineCalificacionModule,
        CineButacaModule,
        CineFuncionModule,
        CineSalaModule,
        CineTicketModule,
        CinePeliculaModule,
        CineEntradaModule,
        CineClienteModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CineEntityModule {}
