import { Moment } from 'moment';
import { IButaca } from 'app/shared/model//butaca.model';
import { IFuncion } from 'app/shared/model//funcion.model';
import { ITicket } from 'app/shared/model//ticket.model';
import { IEntrada } from 'app/shared/model//entrada.model';

export interface IOcupacion {
    id?: number;
    valor?: number;
    created?: Moment;
    updated?: Moment;
    butaca?: IButaca;
    funcion?: IFuncion;
    ticket?: ITicket;
    entrada?: IEntrada;
}

export class Ocupacion implements IOcupacion {
    constructor(
        public id?: number,
        public valor?: number,
        public created?: Moment,
        public updated?: Moment,
        public butaca?: IButaca,
        public funcion?: IFuncion,
        public ticket?: ITicket,
        public entrada?: IEntrada
    ) {}
}
