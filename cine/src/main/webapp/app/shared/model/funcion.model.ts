import { Moment } from 'moment';
import { IOcupacion } from 'app/shared/model//ocupacion.model';
import { ISala } from 'app/shared/model//sala.model';
import { IPelicula } from 'app/shared/model//pelicula.model';

export interface IFuncion {
    id?: number;
    fecha?: Moment;
    created?: Moment;
    updated?: Moment;
    ocupacions?: IOcupacion[];
    sala?: ISala;
    pelicula?: IPelicula;
}

export class Funcion implements IFuncion {
    constructor(
        public id?: number,
        public fecha?: Moment,
        public created?: Moment,
        public updated?: Moment,
        public ocupacions?: IOcupacion[],
        public sala?: ISala,
        public pelicula?: IPelicula
    ) {}
}
