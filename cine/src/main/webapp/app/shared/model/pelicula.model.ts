import { Moment } from 'moment';
import { ICalificacion } from 'app/shared/model//calificacion.model';
import { IFuncion } from 'app/shared/model//funcion.model';

export interface IPelicula {
    id?: number;
    titulo?: string;
    estreno?: Moment;
    created?: Moment;
    updated?: Moment;
    calificacion?: ICalificacion;
    funcions?: IFuncion[];
}

export class Pelicula implements IPelicula {
    constructor(
        public id?: number,
        public titulo?: string,
        public estreno?: Moment,
        public created?: Moment,
        public updated?: Moment,
        public calificacion?: ICalificacion,
        public funcions?: IFuncion[]
    ) {}
}
