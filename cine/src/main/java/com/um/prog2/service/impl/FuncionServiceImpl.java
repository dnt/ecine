package com.um.prog2.service.impl;

import com.um.prog2.service.FuncionService;
import com.um.prog2.domain.Funcion;
import com.um.prog2.repository.FuncionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Funcion.
 */
@Service
@Transactional
public class FuncionServiceImpl implements FuncionService {

    private final Logger log = LoggerFactory.getLogger(FuncionServiceImpl.class);

    private final FuncionRepository funcionRepository;

    public FuncionServiceImpl(FuncionRepository funcionRepository) {
        this.funcionRepository = funcionRepository;
    }

    /**
     * Save a funcion.
     *
     * @param funcion the entity to save
     * @return the persisted entity
     */
    @Override
    public Funcion save(Funcion funcion) {
        log.debug("Request to save Funcion : {}", funcion);        return funcionRepository.save(funcion);
    }

    /**
     * Get all the funcions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Funcion> findAll(Pageable pageable) {
        log.debug("Request to get all Funcions");
        return funcionRepository.findAll(pageable);
    }


    /**
     * Get one funcion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Funcion> findOne(Long id) {
        log.debug("Request to get Funcion : {}", id);
        return funcionRepository.findById(id);
    }

    /**
     * Delete the funcion by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Funcion : {}", id);
        funcionRepository.deleteById(id);
    }
}
