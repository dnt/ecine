package com.um.prog2.service;

import com.um.prog2.domain.Sala;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Sala.
 */
public interface SalaService {

    /**
     * Save a sala.
     *
     * @param sala the entity to save
     * @return the persisted entity
     */
    Sala save(Sala sala);

    /**
     * Get all the salas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Sala> findAll(Pageable pageable);


    /**
     * Get the "id" sala.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Sala> findOne(Long id);

    /**
     * Delete the "id" sala.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
