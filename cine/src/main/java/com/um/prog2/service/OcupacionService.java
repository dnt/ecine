package com.um.prog2.service;

import com.um.prog2.domain.Ocupacion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Ocupacion.
 */
public interface OcupacionService {

    /**
     * Save a ocupacion.
     *
     * @param ocupacion the entity to save
     * @return the persisted entity
     */
    Ocupacion save(Ocupacion ocupacion);

    /**
     * Get all the ocupacions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Ocupacion> findAll(Pageable pageable);


    /**
     * Get the "id" ocupacion.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Ocupacion> findOne(Long id);

    /**
     * Delete the "id" ocupacion.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
