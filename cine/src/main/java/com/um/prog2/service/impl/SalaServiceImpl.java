package com.um.prog2.service.impl;

import com.um.prog2.service.SalaService;
import com.um.prog2.domain.Sala;
import com.um.prog2.repository.SalaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Sala.
 */
@Service
@Transactional
public class SalaServiceImpl implements SalaService {

    private final Logger log = LoggerFactory.getLogger(SalaServiceImpl.class);

    private final SalaRepository salaRepository;

    public SalaServiceImpl(SalaRepository salaRepository) {
        this.salaRepository = salaRepository;
    }

    /**
     * Save a sala.
     *
     * @param sala the entity to save
     * @return the persisted entity
     */
    @Override
    public Sala save(Sala sala) {
        log.debug("Request to save Sala : {}", sala);        return salaRepository.save(sala);
    }

    /**
     * Get all the salas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Sala> findAll(Pageable pageable) {
        log.debug("Request to get all Salas");
        return salaRepository.findAll(pageable);
    }


    /**
     * Get one sala by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Sala> findOne(Long id) {
        log.debug("Request to get Sala : {}", id);
        return salaRepository.findById(id);
    }

    /**
     * Delete the sala by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sala : {}", id);
        salaRepository.deleteById(id);
    }
}
