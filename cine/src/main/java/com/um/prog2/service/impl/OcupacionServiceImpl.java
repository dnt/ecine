package com.um.prog2.service.impl;

import com.um.prog2.service.OcupacionService;
import com.um.prog2.domain.Ocupacion;
import com.um.prog2.repository.OcupacionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Ocupacion.
 */
@Service
@Transactional
public class OcupacionServiceImpl implements OcupacionService {

    private final Logger log = LoggerFactory.getLogger(OcupacionServiceImpl.class);

    private final OcupacionRepository ocupacionRepository;

    public OcupacionServiceImpl(OcupacionRepository ocupacionRepository) {
        this.ocupacionRepository = ocupacionRepository;
    }

    /**
     * Save a ocupacion.
     *
     * @param ocupacion the entity to save
     * @return the persisted entity
     */
    @Override
    public Ocupacion save(Ocupacion ocupacion) {
        log.debug("Request to save Ocupacion : {}", ocupacion);        return ocupacionRepository.save(ocupacion);
    }

    /**
     * Get all the ocupacions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Ocupacion> findAll(Pageable pageable) {
        log.debug("Request to get all Ocupacions");
        return ocupacionRepository.findAll(pageable);
    }


    /**
     * Get one ocupacion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Ocupacion> findOne(Long id) {
        log.debug("Request to get Ocupacion : {}", id);
        return ocupacionRepository.findById(id);
    }

    /**
     * Delete the ocupacion by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Ocupacion : {}", id);
        ocupacionRepository.deleteById(id);
    }
}
