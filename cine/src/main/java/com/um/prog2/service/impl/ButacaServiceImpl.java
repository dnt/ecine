package com.um.prog2.service.impl;

import com.um.prog2.service.ButacaService;
import com.um.prog2.domain.Butaca;
import com.um.prog2.repository.ButacaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Butaca.
 */
@Service
@Transactional
public class ButacaServiceImpl implements ButacaService {

    private final Logger log = LoggerFactory.getLogger(ButacaServiceImpl.class);

    private final ButacaRepository butacaRepository;

    public ButacaServiceImpl(ButacaRepository butacaRepository) {
        this.butacaRepository = butacaRepository;
    }

    /**
     * Save a butaca.
     *
     * @param butaca the entity to save
     * @return the persisted entity
     */
    @Override
    public Butaca save(Butaca butaca) {
        log.debug("Request to save Butaca : {}", butaca);        return butacaRepository.save(butaca);
    }

    /**
     * Get all the butacas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Butaca> findAll(Pageable pageable) {
        log.debug("Request to get all Butacas");
        return butacaRepository.findAll(pageable);
    }


    /**
     * Get one butaca by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Butaca> findOne(Long id) {
        log.debug("Request to get Butaca : {}", id);
        return butacaRepository.findById(id);
    }

    /**
     * Delete the butaca by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Butaca : {}", id);
        butacaRepository.deleteById(id);
    }
}
