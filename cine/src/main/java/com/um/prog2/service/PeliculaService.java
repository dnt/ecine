package com.um.prog2.service;

import com.um.prog2.domain.Pelicula;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Pelicula.
 */
public interface PeliculaService {

    /**
     * Save a pelicula.
     *
     * @param pelicula the entity to save
     * @return the persisted entity
     */
    Pelicula save(Pelicula pelicula);

    /**
     * Get all the peliculas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Pelicula> findAll(Pageable pageable);


    /**
     * Get the "id" pelicula.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Pelicula> findOne(Long id);

    /**
     * Delete the "id" pelicula.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
