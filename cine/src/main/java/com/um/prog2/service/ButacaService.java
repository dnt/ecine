package com.um.prog2.service;

import com.um.prog2.domain.Butaca;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Butaca.
 */
public interface ButacaService {

    /**
     * Save a butaca.
     *
     * @param butaca the entity to save
     * @return the persisted entity
     */
    Butaca save(Butaca butaca);

    /**
     * Get all the butacas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Butaca> findAll(Pageable pageable);


    /**
     * Get the "id" butaca.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Butaca> findOne(Long id);

    /**
     * Delete the "id" butaca.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
