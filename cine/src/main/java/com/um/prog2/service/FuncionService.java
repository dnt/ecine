package com.um.prog2.service;

import com.um.prog2.domain.Funcion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Funcion.
 */
public interface FuncionService {

    /**
     * Save a funcion.
     *
     * @param funcion the entity to save
     * @return the persisted entity
     */
    Funcion save(Funcion funcion);

    /**
     * Get all the funcions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Funcion> findAll(Pageable pageable);


    /**
     * Get the "id" funcion.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Funcion> findOne(Long id);

    /**
     * Delete the "id" funcion.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
