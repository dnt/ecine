/**
 * View Models used by Spring MVC REST controllers.
 */
package com.um.prog2.web.rest.vm;
