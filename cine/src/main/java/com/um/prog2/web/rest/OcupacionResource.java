package com.um.prog2.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.um.prog2.domain.Ocupacion;
import com.um.prog2.service.OcupacionService;
import com.um.prog2.web.rest.errors.BadRequestAlertException;
import com.um.prog2.web.rest.util.HeaderUtil;
import com.um.prog2.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Ocupacion.
 */
@RestController
@RequestMapping("/api")
public class OcupacionResource {

    private final Logger log = LoggerFactory.getLogger(OcupacionResource.class);

    private static final String ENTITY_NAME = "ocupacion";

    private final OcupacionService ocupacionService;

    public OcupacionResource(OcupacionService ocupacionService) {
        this.ocupacionService = ocupacionService;
    }

    /**
     * POST  /ocupacions : Create a new ocupacion.
     *
     * @param ocupacion the ocupacion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ocupacion, or with status 400 (Bad Request) if the ocupacion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ocupacions")
    @Timed
    public ResponseEntity<Ocupacion> createOcupacion(@Valid @RequestBody Ocupacion ocupacion) throws URISyntaxException {
        log.debug("REST request to save Ocupacion : {}", ocupacion);
        if (ocupacion.getId() != null) {
            throw new BadRequestAlertException("A new ocupacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Ocupacion result = ocupacionService.save(ocupacion);
        return ResponseEntity.created(new URI("/api/ocupacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ocupacions : Updates an existing ocupacion.
     *
     * @param ocupacion the ocupacion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ocupacion,
     * or with status 400 (Bad Request) if the ocupacion is not valid,
     * or with status 500 (Internal Server Error) if the ocupacion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ocupacions")
    @Timed
    public ResponseEntity<Ocupacion> updateOcupacion(@Valid @RequestBody Ocupacion ocupacion) throws URISyntaxException {
        log.debug("REST request to update Ocupacion : {}", ocupacion);
        if (ocupacion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Ocupacion result = ocupacionService.save(ocupacion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ocupacion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ocupacions : get all the ocupacions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ocupacions in body
     */
    @GetMapping("/ocupacions")
    @Timed
    public ResponseEntity<List<Ocupacion>> getAllOcupacions(Pageable pageable) {
        log.debug("REST request to get a page of Ocupacions");
        Page<Ocupacion> page = ocupacionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ocupacions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /ocupacions/:id : get the "id" ocupacion.
     *
     * @param id the id of the ocupacion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ocupacion, or with status 404 (Not Found)
     */
    @GetMapping("/ocupacions/{id}")
    @Timed
    public ResponseEntity<Ocupacion> getOcupacion(@PathVariable Long id) {
        log.debug("REST request to get Ocupacion : {}", id);
        Optional<Ocupacion> ocupacion = ocupacionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ocupacion);
    }

    /**
     * DELETE  /ocupacions/:id : delete the "id" ocupacion.
     *
     * @param id the id of the ocupacion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ocupacions/{id}")
    @Timed
    public ResponseEntity<Void> deleteOcupacion(@PathVariable Long id) {
        log.debug("REST request to delete Ocupacion : {}", id);
        ocupacionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
